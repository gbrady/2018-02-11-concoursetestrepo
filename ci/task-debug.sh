#!/bin/sh

echo "Current working directory:"
pwd

echo "Contents of working directory:"
ls -al

echo "dotnet version:"
dotnet --version
